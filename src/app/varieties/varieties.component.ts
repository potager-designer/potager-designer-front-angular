import { Component, OnInit } from '@angular/core';
import { Variety } from '../models/Variety';
import { VarietyService } from '../services/variety.service';
import { CreateVarietyDialogComponent as CreateOrUpdateVarietyDialogComponent } from './create-variety-dialog/create-variety-dialog.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { faTrashAlt, faEdit , faPlusSquare} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-varieties',
  templateUrl: './varieties.component.html',
  styleUrls: ['./varieties.component.css']
})
export class VarietiesComponent implements OnInit {
  deleteIcon = faTrashAlt;
  editIcon = faEdit;
  addIcon = faPlusSquare;
  varieties : Variety[];
  getAllVarieties(){
	   return this.varietyService.getAllVarieties();
  }

  sortVarietyByName(variety1, variety2) {
     if (variety1.name < variety2.name) return -1;
     else if (variety1.name > variety2.name) return 1;
     else return 0;
  }

  constructor(private varietyService: VarietyService, private dialog: MatDialog) { }

  ngOnInit() {
	  this.getAllVarieties().subscribe(varieties => {
      this.varieties = varieties.sort(this.sortVarietyByName);
	  });
  }

  openCreateOrUpdateVarietyDialog(variety: Variety): void {
    const isNew: boolean = !variety;
	if(isNew){
		variety = new Variety({});
	}
	const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '1000px';
    dialogConfig.data = {
	  isNew: isNew,	
      variety: variety,
	  varietyHeritage: undefined
    };
    let dialogRef = this.dialog.open(CreateOrUpdateVarietyDialogComponent, dialogConfig);
    dialogRef.componentInstance.varieties = this.varieties;

    dialogRef.afterClosed().subscribe(result => {
      if(result){
          this.binaryInsertVariety(result, this.varieties, null, null);
      }
    });
  }

  deleteVariety(variety: Variety) {
	return this.varietyService.deleteVariety(variety.id).subscribe((response) => this.varieties = this.varieties.filter(oneVariety => oneVariety.id !== variety.id));
  }

  binaryInsertVariety(variety: Variety, varieties: Variety[], startVal: number, endVal: number){
  	var length = varieties.length;
  	var start = typeof(startVal) != undefined ? startVal : 0;
  	var end = typeof(endVal) != undefined ? endVal : length - 1;
  	var m = start + Math.floor((end - start)/2);

  	if(length == 0){
  		varieties.push(variety);
  		return;
  	}

  	if(variety.name > varieties[end].name){
  		varieties.splice(end + 1, 0, variety);
  		return;
  	}

  	if(variety.name < varieties[start].name){
  		varieties.splice(start, 0, variety);
  		return;
  	}

  	if(start >= end){
  		return;
  	}

  	if(variety.name < varieties[m].name){
  		this.binaryInsertVariety(variety, varieties, start, m - 1);
  		return;
  	}

  	if(variety.name > varieties[m].name){
  		this.binaryInsertVariety(variety, varieties, m + 1, end);
  		return;
  	}
  }

}
