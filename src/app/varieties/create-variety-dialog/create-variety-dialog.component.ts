import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { VarietyService } from '../../services/variety.service';
import { Variety } from '../../models/Variety';
import { ExpositionEnum } from '../../models/ExpositionEnum';

@Component({
  selector: 'app-create-variety-dialog',
  templateUrl: './create-variety-dialog.component.html',
  styleUrls: ['./create-variety-dialog.component.css']
})

export class CreateVarietyDialogComponent implements OnInit {
  isNew: Boolean;
  varieties: Variety[];
  variety: Variety;
  varietyHeritage: Variety;
  monthList: string[] = ['0','1','2','3','4','5','6','7','8','9','10','11'];
  expositionList: ExpositionEnum[] = [ExpositionEnum.FULL_SUN, ExpositionEnum.SUN, ExpositionEnum.SHADE, ExpositionEnum.FULL_SHADE];
  isSaving: Boolean;

  constructor(private varietyService: VarietyService, private dialogRef: MatDialogRef<CreateVarietyDialogComponent>, @Inject(MAT_DIALOG_DATA) data) {
		this.isNew = data.isNew;
    this.variety = data.variety;
		this.varietyHeritage = data.varietyHeritage;
	}

  ngOnInit() {
	  this.isSaving = false;
  }

  cancel() {
    this.dialogRef.close();
  }

  onHeritageVarietyChange(event){
    this.varietyHeritage.id = undefined;
    this.variety = new Variety(this.varietyHeritage);
  }

  saveVariety() {
    this.isSaving = true;
    if(this.isNew){
      this.varietyService.createVariety(this.variety).subscribe((response) => this.onSaveSuccess(response), () => this.onSaveError());
    } else {
      this.varietyService.updateVariety(this.variety).subscribe((response) => this.onSaveSuccess(response), () => this.onSaveError());
    }
  }

  private onSaveSuccess(result) {
    this.isSaving = false;
    this.dialogRef.close(result);
  }

  private onSaveError() {
    this.isSaving = false;
  }
}
