import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPlantToCellDialogComponent } from './add-plant-to-cell-dialog.component';

describe('AddPlantToCellDialogComponent', () => {
  let component: AddPlantToCellDialogComponent;
  let fixture: ComponentFixture<AddPlantToCellDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPlantToCellDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPlantToCellDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
