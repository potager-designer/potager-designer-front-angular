import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { PlantCellRecomandatorService } from '../../../services/plant-cell-recomandator.service';
import { VarietyService } from '../../../services/variety.service';
import { GardenCellService } from '../../../services/garden-cell.service';
import { PlantationService } from '../../../services/plantation.service';
import { PlantationType } from '../../../models/PlantationType';
import { Plantation } from '../../../models/Plantation';
import { VarietySelection } from '../../../models/VarietySelection';
import { Recommandation } from '../../../models/Recommandation';
import { RecommandationEnum } from '../../../models/RecommandationEnum';
import { GardenCell } from '../../../models/GardenCell';

@Component({
  selector: 'add-plant-to-cell-dialog',
  templateUrl: './add-plant-to-cell-dialog.component.html',
  styleUrls: ['./add-plant-to-cell-dialog.component.css']
})
export class AddPlantToCellDialogComponent implements OnInit {
  isSaving: Boolean;
  cellSurface : number;
  gardenCell: GardenCell;
  neighborVarieties: number[];
  recommanded: VarietySelection[];
  neutral: VarietySelection[];
  notRecommanded: VarietySelection[];

  constructor(private plantationService: PlantationService, private gardenCellService: GardenCellService, private varietyService: VarietyService, private plantCellRecomandatorService: PlantCellRecomandatorService, private dialogRef: MatDialogRef<AddPlantToCellDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.gardenCell = data.gardenCell;
      this.cellSurface = data.cellSurface;
      this.neighborVarieties = data.neighborVarieties;
    }

    ngOnInit() {
      this.isSaving = false;
      this.neutral = [];
      this.recommanded = [];
      this.notRecommanded = [];
  	  this.varietyService.getAllVarieties().subscribe(varieties => {
        console.log(varieties);
        for (let i = 0; i < varieties.length; i++) {
          const variety = varieties[i];
          const cellPlantations = this.gardenCell.plantations;
          const varietyPlantation: Plantation = cellPlantations.find(plantation => plantation.variety.name === variety.name);
          let varietySelection : VarietySelection;
          if(varietyPlantation){
            varietySelection = new VarietySelection({occupationMonthNumber: varietyPlantation.occupationMonthNumber, plantationId: varietyPlantation.id, id: variety.id, name: variety.name, selected: true, semis: varietyPlantation.type === PlantationType.SEMIS, quantity:varietyPlantation.quantity, columnSpace: variety.columnSpace, rowSpace: variety.rowSpace, custom: variety.custom});
          } else {
            varietySelection = new VarietySelection({occupationMonthNumber: variety.occupationMonthNumber, id: variety.id, name: variety.name, selected: false, semis: false, quantity:0, columnSpace: variety.columnSpace, rowSpace: variety.rowSpace, custom: variety.custom});
          }
          const recommandationList: Recommandation[] = this.plantCellRecomandatorService.getVarietyRecommandation(variety, this.cellSurface, this.gardenCell, this.neighborVarieties, []);
          varietySelection.recommandations = recommandationList;
           if (recommandationList.some(recommandation => recommandation.value === RecommandationEnum.NotRecommanded)){
               this.notRecommanded.push(varietySelection);
          } else if (recommandationList.every(recommandation => recommandation.value === RecommandationEnum.Neutral)){
              this.neutral.push(varietySelection);
          } else {
              this.recommanded.push(varietySelection);
          }
        }
        this.recommanded.sort(this.sortByCustomAndName);
        this.notRecommanded.sort(this.sortByCustomAndName);
        this.neutral.sort(this.sortByCustomAndName);
  	  });
    }

    sortByCustomAndName(variety1, variety2) {
       if(variety1.custom && !variety2.custom){
         return -1;
       } else if (!variety1.custom && variety2.custom){
         return 1;
       } else {
         if (variety1.name < variety2.name) return -1;
         else if (variety1.name > variety2.name) return 1;
         else return 0;
       }
    }

    cancel() {
      this.dialogRef.close();
    }

    savePlantCell(){
      this.isSaving = true;
      const plantations = [];
      for (let i = 0; i < this.recommanded.length; i++) {
        if(this.recommanded[i].selected && this.recommanded[i].quantity>0){
          const plantation = this.recommanded[i].convertToPlantation();
          plantation.gardenCell.id = this.gardenCell.id;
          plantations.push(plantation);
        }
      }
      for (let i = 0; i < this.neutral.length; i++) {
        if(this.neutral[i].selected && this.neutral[i].quantity>0){
          const plantation = this.neutral[i].convertToPlantation();
          plantation.gardenCell.id = this.gardenCell.id;
          plantations.push(plantation);
        }
      }
      for (let i = 0; i < this.notRecommanded.length; i++) {
        if(this.notRecommanded[i].selected && this.notRecommanded[i].quantity>0){
          const plantation = this.notRecommanded[i].convertToPlantation();
          plantation.gardenCell.id = this.gardenCell.id;
          plantations.push(plantation);
        }
      }

      this.isSaving = true;
      this.gardenCell.plantations = plantations;
      if (!this.gardenCell.id) {
        this.gardenCellService.createGardenCell(this.gardenCell).subscribe((response) => this.onSaveSuccess(response), () => this.onSaveError());
      } else {
        this.gardenCellService.updateGardenCell(this.gardenCell).subscribe((response) => this.onSaveSuccess(this.gardenCell), () => this.onSaveError());
      }
    }

  	private onSaveSuccess(result) {
  		this.isSaving = false;
      this.dialogRef.close(result);
  	}

  	private onSaveError() {
  		this.isSaving = false;
      this.dialogRef.close();
  	}

    isCellOverloaded(){
      return this.getCellOccupation()>this.cellSurface*10000;
    }
    getCellOccupation(){
      let occupation = 0;
      for (let i = 0; i < this.recommanded.length; i++) {
        const varietySelection = this.recommanded[i];
        if(varietySelection.selected && varietySelection.quantity>0){
          occupation += varietySelection.columnSpace * varietySelection.rowSpace * varietySelection.quantity;
        }
      }
      for (let i = 0; i < this.neutral.length; i++) {
        const varietySelection = this.neutral[i];
        if(varietySelection.selected && varietySelection.quantity>0){
          occupation += varietySelection.columnSpace * varietySelection.rowSpace * varietySelection.quantity;
        }
      }
      for (let i = 0; i < this.notRecommanded.length; i++) {
        const varietySelection = this.notRecommanded[i];
        if(varietySelection.selected && varietySelection.quantity>0){
          occupation += varietySelection.columnSpace * varietySelection.rowSpace * varietySelection.quantity;
        }
      }
      return occupation;
    }
}
