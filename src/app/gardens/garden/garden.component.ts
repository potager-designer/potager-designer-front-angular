import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Garden } from '../../models/Garden';
import { GardenCell } from '../../models/GardenCell';
import { Plantation } from '../../models/Plantation';
import { GardenService } from '../../services/garden.service';
import { GardenCellService } from '../../services/garden-cell.service';
import { AddPlantToCellDialogComponent } from './add-plant-to-cell-dialog/add-plant-to-cell-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-garden',
  templateUrl: './garden.component.html',
  styleUrls: ['./garden.component.css']
})

export class GardenComponent implements OnInit {
  isDraggingCompass = false;
  garden: Garden;
  gardenCells: GardenCell[] = [];
  private sub: any;
  year: number;
  month: number;

  constructor(private gardenService: GardenService, private gardenCellService: GardenCellService, private route: ActivatedRoute, private dialog: MatDialog) { }

  ngOnInit() {
	  this.sub = this.route.params.subscribe(params => {
        const id = +params['id']; // (+) converts string 'id' to a number
        this.year = (new Date()).getFullYear();
        this.month = (new Date()).getMonth();
  	    this.gardenService.getOneGarden(id).subscribe(garden => {
           this.garden = garden;
           this.setGardenCells()
  	   });
    });
  }

  setGardenCells(): void{
    this.gardenCellService.getGardenCellsByMonth(this.garden.id, this.month, this.year).subscribe(gardenCells => {
      this.gardenCells=[];
      for(let rowIndex=0;rowIndex<this.garden.nbRow;rowIndex++){
        for(let columnIndex=0;columnIndex<this.garden.nbColumn;columnIndex++){
            const existingGardenCell = gardenCells.find(gardenCell => gardenCell.rowNb === rowIndex && gardenCell.columnNb === columnIndex);
            if(existingGardenCell){
              this.gardenCells.push(
                new GardenCell(existingGardenCell)
                );
            } else {
              const newGardenCell = new GardenCell({gardenId:this.garden.id,rowNb: rowIndex, columnNb: columnIndex, month: this.month, year:this.year, plantations: []})
              this.gardenCells.push(newGardenCell);
            }

            this.setCellExposition(rowIndex, columnIndex);
        }
      }
     }
   );
  }

  setCellExposition(rowIndex: number, columnIndex: number){
    this.gardenService.getGardenCellExposition(this.garden.id, rowIndex, columnIndex, this.month, this.year).subscribe(
      exposition => {
        console.log("Expo for cell ("+ rowIndex+","+columnIndex + ") is "+exposition );
        this.gardenCells[rowIndex*this.garden.nbColumn+columnIndex].exposition = exposition;
      }
    );
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getMonthName(){
    const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    return monthNames[this.month];
  }

  startDragingCompass(event: MouseEvent){
    this.isDraggingCompass= true;
  }

  stopDragingCompass(event: MouseEvent){
    if(this.isDraggingCompass ){
      this.isDraggingCompass = false;
      this.gardenService.updateNorthAngle(this.garden.id, this.garden.northAngleWithTopPageInDegree).subscribe(
        result => {
        for(let rowIndex=0;rowIndex<this.garden.nbRow;rowIndex++){
          for(let columnIndex=0;columnIndex<this.garden.nbColumn;columnIndex++){
            this.setCellExposition(rowIndex, columnIndex);
          }
        }
      })
    }
  }

  moveCompass(event: MouseEvent){
    if(this.isDraggingCompass){
        const clickX = event.pageX;
        const clickY = event.pageY;
        console.log("clickX:"+clickX);
        console.log("clickY:"+clickY);

        const srcElement =  event.srcElement as HTMLImageElement
        const imgX = srcElement.x;
        const imgWidth = srcElement.clientWidth;
        console.log("imgX:"+imgX);
        console.log("imgWidth:"+imgWidth);
        const imgY = srcElement.y;
        const imgHeight = srcElement.clientHeight;
        console.log("imgY:"+imgY);
        console.log("imgHeight:"+imgHeight);
        const compassImageCenterX = imgX + imgWidth/2;
        const compassImageCenterY = imgY + imgHeight/2;
        console.log("centerX:"+compassImageCenterX);
        console.log("centerY:"+compassImageCenterY);

        const angle = Math.atan2(clickX - compassImageCenterX,-(clickY- compassImageCenterY))*180 / Math.PI;
        console.log("angle:"+angle);
        this.garden.northAngleWithTopPageInDegree = angle;
    }
  }

  openAddPlantToCellDialog(index: number): void {
    const selectedCell: GardenCell = this.gardenCells[index];
    const neighborVarieties = this.getNeighbourVarieties(index);

    let dialogRef = this.dialog.open(AddPlantToCellDialogComponent, {
      width: '800px',
      height: '700px',
      data: { cellSurface :this.garden.getCellSurface(), gardenCell: selectedCell, neighborVarieties: neighborVarieties}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.setGardenCell(index, result);
      }
    });
  }

  getPlantationVarieties(index: number){
    return this.gardenCells[index].getPlantationVarieties();
  }

  setGardenCell(index: number, gardenCell: GardenCell){
    this.gardenCells[index] = gardenCell;
  }

  getNeighbourVarieties(index: number): number[]{
    let neighborVarieties= this.getPlantationVarieties(index)
    if(index%this.garden.nbColumn !== 0){
      neighborVarieties = neighborVarieties.concat(this.getPlantationVarieties(index-1));
      if(Math.floor(index/this.garden.nbColumn) !== 0){
        neighborVarieties = neighborVarieties.concat(this.getPlantationVarieties(index-this.garden.nbColumn-1));
      }
      if(Math.floor(index/this.garden.nbColumn)+1 !== this.garden.nbRow ){
        neighborVarieties = neighborVarieties.concat(this.getPlantationVarieties(index+this.garden.nbColumn-1));
      }
    }
    if(index%this.garden.nbColumn+1 !==this.garden.nbColumn){
      neighborVarieties = neighborVarieties.concat(this.getPlantationVarieties(index+1));
      if(Math.floor(index/this.garden.nbColumn) !== 0){
        neighborVarieties = neighborVarieties.concat(this.getPlantationVarieties(index-this.garden.nbColumn+1));
      }
      if(Math.floor(index/this.garden.nbColumn)+1 !== this.garden.nbRow ){
        neighborVarieties = neighborVarieties.concat(this.getPlantationVarieties(index+this.garden.nbColumn+1));
      }
    }
    if(Math.floor(index/this.garden.nbColumn) !== 0){
      neighborVarieties = neighborVarieties.concat(this.getPlantationVarieties(index-this.garden.nbColumn));
    }
    if(Math.floor(index/this.garden.nbColumn)+1 !== this.garden.nbRow ){
      neighborVarieties = neighborVarieties.concat(this.getPlantationVarieties(index+this.garden.nbColumn));
    }
    return neighborVarieties;
  }

  previousMonth(){
    if(this.month === 0){
      this.month = 11;
      this.year -=1;
    } else {
      this.month -= 1;
    }
    this.setGardenCells();
  }

  nextMonth(){
    if(this.month === 11){
      this.month = 0;
      this.year +=1;
    } else {
      this.month += 1;
    }
    this.setGardenCells();
  }
}
