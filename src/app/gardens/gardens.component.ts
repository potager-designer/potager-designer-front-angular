import { Component, OnInit } from '@angular/core';
import { Garden } from '../models/Garden';
import { GardenService } from '../services/garden.service';
import { CreateGardenDialogComponent } from './create-garden-dialog/create-garden-dialog.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

@Component({
  selector: 'app-gardens',
  templateUrl: './gardens.component.html',
  styleUrls: ['./gardens.component.css']
})
export class GardensComponent implements OnInit {
  gardens : Garden[];
  result : Garden;
  getAllGardens(){
	   return this.gardenService.getAllGardens();
  }

  editGarden(garden){
     return this.gardenService.updateGarden(garden).subscribe((response) => this.onSaveSuccess(), () => this.onSaveError());
  }

  deleteGarden(garden){
     return this.gardenService.deleteGarden(garden).subscribe((response) => this.onSaveSuccess(), () => this.onSaveError());
  }

  constructor(private gardenService: GardenService, private dialog: MatDialog) {}

  ngOnInit() {
    this.loadGardens()
  }

  loadGardens(): void {
    this.gardenService.getAllGardens().subscribe(gardens => {
      this.gardens = [new Garden({})];
      this.gardens = this.gardens.concat(gardens);
    });
  }

  openCreateGardenDialog(garden): void {
    const isNew: boolean = !garden;
    if(isNew){
      garden = {northAngleWithTopPageInDegree: 0, topWall:0, leftWall:0, rightWall:0, bottomWall:0};
    }
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '1000px';
    dialogConfig.data = {
      garden: garden
    };
    let dialogRef = this.dialog.open(CreateGardenDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => this.loadGardens());
  }

  private onSaveSuccess() {
    this.loadGardens();
  }

  private onSaveError() {
  }

}
