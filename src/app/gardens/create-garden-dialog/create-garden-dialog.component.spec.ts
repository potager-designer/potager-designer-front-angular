import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateGardenDialogComponent } from './create-garden-dialog.component';

describe('CreateGardenDialogComponent', () => {
  let component: CreateGardenDialogComponent;
  let fixture: ComponentFixture<CreateGardenDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateGardenDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateGardenDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
