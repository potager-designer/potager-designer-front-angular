import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { GardenService } from '../../services/garden.service';
import { Garden } from '../../models/Garden';

@Component({
  selector: 'create-garden-dialog',
  templateUrl: './create-garden-dialog.component.html',
  styleUrls: ['./create-garden-dialog.component.css']
})
export class CreateGardenDialogComponent implements OnInit {
  garden: any;
  isSaving: Boolean;
  constructor(private gardenService: GardenService, private dialogRef: MatDialogRef<CreateGardenDialogComponent>, @Inject(MAT_DIALOG_DATA) data) {
		this.garden = data.garden;
	}

  ngOnInit() {
	  this.isSaving = false;
  }

	cancel() {
		this.dialogRef.close();
	}

	saveGarden() {
		this.isSaving = true;
		this.gardenService.createGarden(new Garden(this.garden)).subscribe((response) => this.onSaveSuccess(response), () => this.onSaveError());
	}

	private onSaveSuccess(result) {
		this.isSaving = false;
		this.dialogRef.close(result);
	}

	private onSaveError() {
		this.isSaving = false;
	}
}
