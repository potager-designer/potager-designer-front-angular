import { Injectable } from '@angular/core';
import { Plantation } from '../models/Plantation';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class PlantationService {
  private baseUrl: string = 'http://localhost:8081/api/plantations';
  private optionsHeaders: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http : HttpClient) { }

  createPlantation(plantation: Plantation): Observable<Plantation>{
    return this.http
    .post<Plantation>(`${this.baseUrl}`, plantation, {headers: this.optionsHeaders})
    .pipe(map(plantation => new Plantation(plantation)));
  }

  updatePlantation(plantation: Plantation): Observable<{}>{
    return this.http
    .put(`${this.baseUrl}/${plantation.id}`, plantation, {headers: this.optionsHeaders});
  }

  deletePlantation(plantationId: number): Observable<{}>{
    return this.http
    .delete(`${this.baseUrl}/${plantationId}`);
  }

}
