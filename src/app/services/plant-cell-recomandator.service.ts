import { Injectable } from '@angular/core';
import { RecommandationEnum } from '../models/RecommandationEnum';
import { Variety } from '../models/Variety';
import { GardenCell } from '../models/GardenCell';
import { Recommandation } from '../models/Recommandation';
import { VarietyService } from './variety.service';

@Injectable()
export class PlantCellRecomandatorService {

  constructor(private varietyService: VarietyService) { }

  getVarietyRecommandation(variety: Variety, cellSurface: number, cell: GardenCell, neighborVarieties: number[], previousPlant: string[]):Recommandation[] {
    //TODO: consider over time occupation ?
    const recommandationList = [];
    const neighborEnnemies = variety.neighborEnnemies.concat(variety.mirrorNeighborEnnemies);
    const neighborFriends = variety.neighborFriends.concat(variety.mirrorNeighborFriends);
    if(neighborVarieties.some(neighborVariety => neighborEnnemies.includes(neighborVariety))){
      recommandationList.push(new Recommandation({value:RecommandationEnum.NotRecommanded, reason:"Some neighbor ennemies"}));
    }
    if(neighborVarieties.some(neighborVariety => neighborFriends.includes(neighborVariety))){
      recommandationList.push(new Recommandation({value:RecommandationEnum.Recommanded, reason:"Some neighbor friends"}));
    }
    if(cell.exposition === variety.bestExposition){
      recommandationList.push(new Recommandation({value:RecommandationEnum.Neutral, reason:"Good exposition"}));
    } else {
      recommandationList.push(new Recommandation({value:RecommandationEnum.NotRecommanded, reason:"Bad exposition"}));
    }
    if(variety.plantationMonths.indexOf(cell.month + 1 )!==-1 || variety.semisMonths.indexOf(cell.month + 1)!==-1){
      recommandationList.push(new Recommandation({value:RecommandationEnum.Neutral, reason:"Good month for plantation or semis"}));
    } else {
      recommandationList.push(new Recommandation({value:RecommandationEnum.NotRecommanded, reason:"Not good month for plantation or semis"}));
    }
    return recommandationList;
  }
}
