import { TestBed, inject } from '@angular/core/testing';

import { PlantationService } from './plantation.service';

describe('PlantationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PlantationService]
    });
  });

  it('should be created', inject([PlantationService], (service: PlantationService) => {
    expect(service).toBeTruthy();
  }));
});
