import { Injectable } from '@angular/core';
import { Garden } from '../models/Garden';
import { Observable } from 'rxjs/Observable';
import { ExpositionEnum } from '../models/ExpositionEnum';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class GardenService {
  private baseUrl: string = 'http://localhost:8081/api/gardens';
  private optionsHeaders: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http : HttpClient) { }

  getAllGardens(): Observable<Garden[]>{
    return this.http
      .get<Garden[]>(`${this.baseUrl}`).pipe(map(gardens => gardens.map(garden => new Garden(garden))));

  }

  getOneGarden(id: number): Observable<Garden>{
     return this.http
       .get<Garden>(`${this.baseUrl}/${id}`)
       .pipe( 
         map(
          garden => { return new Garden(garden)}
         )
       )
  }

  getGardenCellExposition(gardenId: number, rowNb: number, columnNb: number, month: number, year: number): Observable<ExpositionEnum>{
    return this.http
      .get<ExpositionEnum>(`${this.baseUrl}/${gardenId}/cellExposition?rowNb=${rowNb}&columnNb=${columnNb}&year=${year}&month=${month}`)       
  }

  createGarden(garden: Garden): Observable<Garden>{
    return this.http
    .post<Garden>(`${this.baseUrl}`, garden, {headers: this.optionsHeaders})       
    .pipe( 
      map(
       garden => { return new Garden(garden)}
      )
    )
  }

  deleteGarden(garden: Garden): Observable<{}>{
    return this.http
    .delete<Garden>(`${this.baseUrl}/${garden.id}`, {headers: this.optionsHeaders})
  }

  updateGarden(garden: Garden): Observable<{}>{
    return this.http
    .put<Garden>(`${this.baseUrl}/${garden.id}`, garden, {headers: this.optionsHeaders});
  }

  updateNorthAngle(gardenId: number, northAngleWithTopPageInDegree : number): Observable<{}>{
      return this.http
      .put<number>(`${this.baseUrl}/${gardenId}/northAngle`, northAngleWithTopPageInDegree, {headers: this.optionsHeaders});
  }
}
