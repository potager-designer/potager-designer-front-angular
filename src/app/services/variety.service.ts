import { Injectable } from '@angular/core';
import { Variety } from '../models/Variety';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class VarietyService {
  private baseUrl: string = 'http://localhost:8081/api/varieties';

  constructor(private http : HttpClient) { }

  getAllVarieties(): Observable<Variety[]>{
    return this.http
      .get<Variety[]>(`${this.baseUrl}`).pipe(map(varieties => varieties.map(variety => new Variety(variety))));
  }

  createVariety(variety: Variety): Observable<Variety>{
    return this.http
    .post<Variety>(`${this.baseUrl}`, variety).pipe(map(variety => new Variety(variety)));
  }

   updateVariety(variety: Variety): Observable<Variety>{
     return this.http
     .put<Variety>(`${this.baseUrl}/${variety.id}`, variety).pipe(map(variety => new Variety(variety)));
   }

   deleteVariety(id: Number) {
    return this.http.delete(`${this.baseUrl}/${id}`)
   }
}
