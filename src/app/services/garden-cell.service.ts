import { Injectable } from '@angular/core';
import { GardenCell } from '../models/GardenCell';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class GardenCellService {
  private baseUrl: string = 'http://localhost:8081/api/gardenCells';
  private optionsHeaders: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http : HttpClient) { }

  getGardenCellsByMonth(gardenId: number, month: number, year: number): Observable<GardenCell[]>{
    return this.http
      .get<GardenCell[]>(`${this.baseUrl}?gardenId=${gardenId}&year=${year}&month=${month}`).pipe(map(gardenCells => gardenCells.map(gardenCell => new GardenCell(gardenCell))));
  }

  createGardenCell(gardenCell: GardenCell): Observable<GardenCell>{
    return this.http
    .post<GardenCell>(`${this.baseUrl}`, gardenCell, {headers: this.optionsHeaders}).pipe(map(gardenCell =>  new GardenCell(gardenCell)));
  }

  updateGardenCell(gardenCell: GardenCell): Observable<GardenCell>{
   return this.http
    .put<GardenCell>(`${this.baseUrl}/${gardenCell.id}`, gardenCell, {headers: this.optionsHeaders}).pipe(map(gardenCell =>  new GardenCell(gardenCell)));;
  }


}
