import { TestBed, inject } from '@angular/core/testing';

import { PlantCellRecomandatorService } from './plant-cell-recomandator.service';

describe('PlantCellRecomandatorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PlantCellRecomandatorService]
    });
  });

  it('should be created', inject([PlantCellRecomandatorService], (service: PlantCellRecomandatorService) => {
    expect(service).toBeTruthy();
  }));
});
