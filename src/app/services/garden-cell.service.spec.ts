import { TestBed, inject } from '@angular/core/testing';

import { GardenCellService } from './garden-cell.service';

describe('GardenCellService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GardenCellService]
    });
  });

  it('should be created', inject([GardenCellService], (service: GardenCellService) => {
    expect(service).toBeTruthy();
  }));
});
