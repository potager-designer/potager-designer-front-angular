import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { GardensComponent } from './gardens/gardens.component';
import { GardenComponent } from './gardens/garden/garden.component';
import { VarietiesComponent } from './varieties/varieties.component';

const routes: Routes = [
  { path: '', component: GardensComponent },
  { path: 'gardens', component: GardensComponent },
  { path: 'garden/:id', component: GardenComponent },
  { path: 'varieties', component: VarietiesComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
