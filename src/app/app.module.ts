import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { GardensComponent } from './gardens/gardens.component';
import { GardenService } from './services/garden.service';
import { VarietyService } from './services/variety.service';
import { PlantCellRecomandatorService } from './services/plant-cell-recomandator.service';
import { AppRoutingModule } from './/app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BannerComponent } from './banner/banner.component';
import { CreateGardenDialogComponent } from './gardens/create-garden-dialog/create-garden-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { MatNativeDateModule } from '@angular/material/core'
import { GardenComponent } from './gardens/garden/garden.component';
import { AddPlantToCellDialogComponent } from './gardens/garden/add-plant-to-cell-dialog/add-plant-to-cell-dialog.component';
import { HttpClientModule } from '@angular/common/http';
import { GardenCellService } from './services/garden-cell.service';
import { PlantationService } from './services/plantation.service';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';
import { VarietiesComponent } from './varieties/varieties.component';
import { CreateVarietyDialogComponent } from './varieties/create-variety-dialog/create-variety-dialog.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    AppComponent,
    GardensComponent,
    BannerComponent,
    CreateGardenDialogComponent,
    GardenComponent,
    AddPlantToCellDialogComponent,
    SafeHtmlPipe,
    VarietiesComponent,
    CreateVarietyDialogComponent
  ],
  imports: [
    BrowserModule,
  	BrowserAnimationsModule,
  	FormsModule,
  	AppRoutingModule,
    HttpClientModule,
  	MatCardModule,
  	MatGridListModule,
  	MatDialogModule,
  	MatButtonModule,
  	MatFormFieldModule,
    MatInputModule,
  	MatListModule,
    MatToolbarModule,
    MatTabsModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    FontAwesomeModule
  ],
  entryComponents: [ CreateGardenDialogComponent, AddPlantToCellDialogComponent, CreateVarietyDialogComponent],
  providers: [GardenService, PlantCellRecomandatorService, VarietyService, GardenCellService, PlantationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
