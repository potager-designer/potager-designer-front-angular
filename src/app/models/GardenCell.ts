import { Plantation } from './Plantation';
import { ExpositionEnum } from './ExpositionEnum';

export class GardenCell {
  id: number;
  gardenId : number;
  month: number;
  year: number;
  rowNb : number;
  columnNb : number;
  plantations: Plantation[];
  exposition : ExpositionEnum;

  constructor(gardenCellInfo:any) {
      this.id = gardenCellInfo.id;
      this.gardenId = gardenCellInfo.gardenId;
      this.plantations = gardenCellInfo.plantations.map(plantation => new Plantation(plantation));
      this.exposition = gardenCellInfo.exposition;
      this.month = gardenCellInfo.month;
      this.year = gardenCellInfo.year;
      this.columnNb = gardenCellInfo.columnNb;
      this.rowNb = gardenCellInfo.rowNb;
  }

  getPlantationVarieties() {
    if(this.plantations && this.plantations.length > 0){
      return this.plantations.map(plantation => plantation.variety.id)
    } else return [];
  }

  getPlantationVarietyNames() {
    if(this.plantations && this.plantations.length > 0){
      return this.plantations.map(plantation => plantation.variety.name)
    } else return [];
  }
}
