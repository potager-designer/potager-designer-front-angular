import { PlantationType } from './PlantationType';
import { Plantation } from './Plantation';
import { Recommandation } from './Recommandation';
import { RecommandationEnum } from './RecommandationEnum';
import { Variety } from './Variety';

export class VarietySelection {
  id : number;
  name : string;
  selected: boolean;
  semis: boolean;
  quantity: number;
  columnSpace: number;
  rowSpace: number;
  plantationId: number;
  recommandations: Recommandation[];
  occupationMonthNumber : number;
  custom: boolean;

  constructor(varietySelectionInfo:any) {
      this.id = varietySelectionInfo.id;
      this.name = varietySelectionInfo.name;
      this.selected = varietySelectionInfo.selected;
      this.semis = varietySelectionInfo.semis;
      this.quantity = varietySelectionInfo.quantity;
      this.columnSpace = varietySelectionInfo.columnSpace;
      this.rowSpace = varietySelectionInfo.rowSpace;
      this.plantationId = varietySelectionInfo.plantationId;
      this.occupationMonthNumber = varietySelectionInfo.occupationMonthNumber;
      this.custom = varietySelectionInfo.custom;
  }

  convertToPlantation(){
    const type = this.semis ? PlantationType.SEMIS: PlantationType.PLANT;
    return new Plantation({variety: new Variety({id: this.id, name: this.name}), id : this.plantationId, type: type, quantity: this.quantity, occupationMonthNumber: this.occupationMonthNumber, gardenCell:{}});
  }

  getTooltipReasonWhy(): String{
    let result = "";
    if(this.recommandations){
      for(let i=0; i<this.recommandations.length;i++){
        if(this.recommandations[i].value === RecommandationEnum.Recommanded){
          result += "<span style='color: green;'>"+this.recommandations[i].reason+"</span>";
        } else if(this.recommandations[i].value === RecommandationEnum.NotRecommanded){
          result += "<span style='color: red;'>"+this.recommandations[i].reason+"</span>";
        } else {
          result += "<span>"+this.recommandations[i].reason+"</span>";
        }
        result +=  ", ";
      }
    }
    return result;
  }
}
