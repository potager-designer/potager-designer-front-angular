export enum ExpositionEnum {
    FULL_SUN = 0,
    SUN = 1,
    SHADE = 2,
    FULL_SHADE = 3
}
