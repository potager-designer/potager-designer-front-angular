import { PlantationType } from './PlantationType';
import { Variety } from './Variety';
import { GardenCell } from './GardenCell';

export class Plantation {
  id: number;
  gardenCell: GardenCell;
  variety : Variety;
  date: Date;
  type: PlantationType;
  quantity: number;
  occupationMonthNumber: number;

  constructor(plantationInfo:any) {
      this.id = plantationInfo.id;
      this.gardenCell = plantationInfo.gardenCell;
      this.variety = plantationInfo.variety;
      this.date = plantationInfo.date;
      this.type = plantationInfo.type;
      this.quantity = plantationInfo.quantity;
      this.occupationMonthNumber = plantationInfo.occupationMonthNumber;
  }
}
