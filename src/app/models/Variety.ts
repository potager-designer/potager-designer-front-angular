import { ExpositionEnum } from './ExpositionEnum';

export class Variety {
  id : number;
  name : string;
  bestExposition: ExpositionEnum;
  columnSpace: number;
  rowSpace: number;
  neighborEnnemies: number[];
  neighborFriends: number[];
  mirrorNeighborEnnemies: number[];
  mirrorNeighborFriends: number[];
  rotation: number[];
  plantationMonths: number[];
  semisMonths: number[];
  occupationMonthNumber: number;
  custom: Boolean;

  constructor(varietyInfo:any) {
      this.id = varietyInfo.id;
      this.name = varietyInfo.name;
      this.bestExposition = varietyInfo.bestExposition;
      this.columnSpace = varietyInfo.columnSpace;
      this.rowSpace = varietyInfo.rowSpace;
      this.neighborEnnemies = varietyInfo.neighborEnnemies;
      this.neighborFriends = varietyInfo.neighborFriends;
      this.mirrorNeighborEnnemies = varietyInfo.mirrorNeighborEnnemies;
      this.mirrorNeighborFriends = varietyInfo.mirrorNeighborFriends;
      this.rotation = varietyInfo.rotation;
      this.plantationMonths = varietyInfo.plantationMonths;
      this.semisMonths = varietyInfo.semisMonths;
      this.occupationMonthNumber = varietyInfo.occupationMonthNumber;
      this.custom = varietyInfo.custom;
  }
}
