import { RecommandationEnum } from './RecommandationEnum';
export class Recommandation {
  value : RecommandationEnum;
  reason : String;

  constructor(recommandationInfo:any) {
      this.value = recommandationInfo.value;
      this.reason = recommandationInfo.reason;
  }
}
