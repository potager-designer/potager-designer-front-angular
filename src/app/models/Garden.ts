export class Garden {
  id: number;
  name: string;
  nbRow: number;
  nbColumn: number;
  nbCells: number;
  cellLength: number;
  topWall: number;
  bottomWall: number;
  rightWall: number;
  leftWall: number;
  northAngleWithTopPageInDegree : number;

  constructor(gardenInfo:any) {
      this.id = gardenInfo.id;
      this.name = gardenInfo.name;
      this.nbRow = gardenInfo.nbRow;
      this.nbColumn = gardenInfo.nbColumn;
      this.cellLength = gardenInfo.cellLength;
      this.topWall = gardenInfo.topWall;
      this.bottomWall = gardenInfo.bottomWall;
      this.rightWall = gardenInfo.rightWall;
      this.leftWall = gardenInfo.leftWall;
      this.northAngleWithTopPageInDegree = gardenInfo.northAngleWithTopPageInDegree;
      this.nbCells = this.calculateNbCells();
    }

  calculateNbCells() {
    if(this.nbRow && this.nbColumn){
      return this.nbRow * this.nbColumn;
    } else {
      return 0;
    }
  }

  getCellSurface(){
    return this.cellLength*this.cellLength;
  }
}
