FROM node:12.16.1 as build
WORKDIR /app
COPY package*.json /app/
RUN npm install -g @angular/cli@1.7.4
RUN npm install
COPY . /app
RUN ng build --outputPath=/app/dist/out/

# Stage 2, use the compiled app, ready for production with Nginx
FROM nginx
COPY --from=build /app/dist/out/ /usr/share/nginx/html
COPY /nginx-custom.conf /etc/nginx/conf.d/default.conf